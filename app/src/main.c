
#include "chip.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"


xSemaphoreHandle s1;

xQueueHandle q1;
xQueueHandle q2;

/* Tarea 1 */
static void Task1(void *pvParameters)
{

	uint32_t round_count;

	while(1)
	{
		/* Tomo el semaforo (contador) */
		xSemaphoreTake(s1,portMAX_DELAY);

		/* Leo el valor de la cola */
		xQueueReceive(q1, &round_count, portMAX_DELAY);

		/* Incremento el valor */
		round_count++;

		/* Envio el dato la cola */
		xQueueOverwrite(q2, &round_count);

		vTaskDelay(500 / portTICK_RATE_MS);


	}
}


/* Tarea 2 */
static void Task2(void *pvParameters)
{

	uint32_t value;

	while(1)
	{

		/* Leo el valor de la cola */
		xQueuePeek(q2, &value, portMAX_DELAY);

		/* Envio el dato la cola */
		xQueueSendToBack(q1, &value,portMAX_DELAY);

		if(value<6)
		xSemaphoreGive(s1);

	}
}


/* Tarea 3 */
static void Task3(void *pvParameters)
{
	uint32_t a=0;

	while(1)
	{
		xQueuePeek(q2, &a, portMAX_DELAY);
		if(a>4){

		/* Enciendo el led */
		Chip_GPIO_SetPinState(LPC_GPIO, 0, 22, true);
		}
	}
}




/****************************************************************************************************/
/**************************************** MAIN ******************************************************/
/****************************************************************************************************/

int main(void)
{
	SystemCoreClockUpdate();

	//s1 = xSemaphoreCreateCounting(5,1);
	vSemaphoreCreateBinary(s1);


	uint32_t initial_value=0;

	q1 = xQueueCreate(1,sizeof(uint32_t));
	xQueueSendToBack(q1, &initial_value, portMAX_DELAY);

	q2 = xQueueCreate(1,sizeof(uint32_t));
	xQueueSendToBack(q2, &initial_value, portMAX_DELAY);

	Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, 22);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, 22, false);


    /* Creacion de tareas */
	xTaskCreate(Task1, (char *) "Tarea 1",
    			configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 3UL),
    			(xTaskHandle *) NULL);
    xTaskCreate(Task2, (char *) "Tarea 2",
        		configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 2UL),
        		(xTaskHandle *) NULL);
    xTaskCreate(Task3, (char *) "Tarea 3",
            	configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
            	(xTaskHandle *) NULL);


    /* Start the scheduler */
	vTaskStartScheduler();

	/* Nunca debería arribar aquí */

    return 0;
}

